/** Created by Nicholas Hillier on 2015-04-12. */
(function(){
    'use strict';

    sparkwood.controller('HomeController', HomeController);
    HomeController.$inject = ['$scope', 'messageService'];

    function HomeController($scope, messageService) {
        $scope.data = {};
        $scope.data.message = messageService.getMsg();
    }

})();